
```
digraph finite_state_machine {
	rankdir=LR;
	size="8,5"
	node [shape = none]; "";
	node [shape = doublecircle]; 1
	node [shape = circle];
	"" -> 9
	2 -> 1 [ label = "b, c, d" ];
	9 -> 8 [ label = "f" ];
	3 -> 2 [ label = "b, c, d" ];
	9 -> 4 [ label = "h" ];
	6 -> 3 [ label = "b, c, d" ];
	9 -> 1 [ label = "a" ];
	9 -> 2 [ label = "e" ];
	7 -> 3 [ label = "c, d" ];
	9 -> 7 [ label = "j" ];
	4 -> 2 [ label = "b, c" ];
	9 -> 6 [ label = "k" ];
	9 -> 5 [ label = "l" ];
	8 -> 2 [ label = "d" ];
	9 -> 3 [ label = "g, i" ];
	5 -> 3 [ label = "b" ];
}
```
