package io.gitlab.wulflex;

import java.io.IOException;

public interface CodePointReader {

	int read() throws IOException;

}
