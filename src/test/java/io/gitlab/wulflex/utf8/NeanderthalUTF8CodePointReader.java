package io.gitlab.wulflex.utf8;

import io.gitlab.wulflex.CodePointReader;

import java.io.IOException;
import java.io.InputStream;

public class NeanderthalUTF8CodePointReader implements CodePointReader {

	private final InputStream in;

	public NeanderthalUTF8CodePointReader(InputStream in) {
		this.in = in;
	}

	@Override
	public int read() throws IOException {
		int b0 = in.read();

		if (b0 == -1) {
			return -1;
		}

		if (isInRange(b0, 0x00, 0x7f)) {
			return b0;
		}

		if (isInRange(b0, 0xc2, 0xdf)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x80, 0xbf)) {
				return ((b0 & 0x1f) <<  6) | (b1 & 0x3f);
			}
			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xe0, 0xe0)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0xa0, 0xbf)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					return ((b0 & 0x0f) << 12) | ((b1 & 0x3f) <<  6) | (b2 & 0x3f);
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xe1, 0xec)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x80, 0xbf)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					return ((b0 & 0x0f) << 12) | ((b1 & 0x3f) <<  6) | (b2 & 0x3f);
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xed, 0xed)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x80, 0x9f)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					return ((b0 & 0x0f) << 12) | ((b1 & 0x3f) <<  6) | (b2 & 0x3f);
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xee, 0xef)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x80, 0xbf)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					return ((b0 & 0x0f) << 12) | ((b1 & 0x3f) <<  6) | (b2 & 0x3f);
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xf0, 0xf0)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x90, 0xbf)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					int b3 = in.read();
					if (b3 == -1) {
						throw new IOException("Unexpected end of stream");
					}

					if (isInRange(b3, 0x80, 0xbf)) {
						return ((b0 & 0x07) << 18) | ((b1 & 0x3f) << 12) | ((b2 & 0x3f) <<  6) | (b3 & 0x3f);
					}

					throw new IOException("Incorrect UTF-8 encoding");
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xf1, 0xf3)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x80, 0xbf)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					int b3 = in.read();
					if (b3 == -1) {
						throw new IOException("Unexpected end of stream");
					}

					if (isInRange(b3, 0x80, 0xbf)) {
						return ((b0 & 0x07) << 18) | ((b1 & 0x3f) << 12) | ((b2 & 0x3f) <<  6) | (b3 & 0x3f);
					}

					throw new IOException("Incorrect UTF-8 encoding");
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		if (isInRange(b0, 0xf4, 0xf4)) {
			int b1 = in.read();
			if (b1 == -1) {
				throw new IOException("Unexpected end of stream");
			}

			if (isInRange(b1, 0x80, 0x8f)) {
				int b2 = in.read();
				if (b2 == -1) {
					throw new IOException("Unexpected end of stream");
				}

				if (isInRange(b2, 0x80, 0xbf)) {
					int b3 = in.read();
					if (b3 == -1) {
						throw new IOException("Unexpected end of stream");
					}

					if (isInRange(b3, 0x80, 0xbf)) {
						return ((b0 & 0x07) << 18) | ((b1 & 0x3f) << 12) | ((b2 & 0x3f) <<  6) | (b3 & 0x3f);
					}

					throw new IOException("Incorrect UTF-8 encoding");
				}

				throw new IOException("Incorrect UTF-8 encoding");
			}

			throw new IOException("Incorrect UTF-8 encoding");
		}

		throw new IOException("Invalid UTF8 sequence");
	}

	private static boolean isInRange(int codePoint, int min, int max) {
		return min <= codePoint && codePoint <= max;
	}

}
