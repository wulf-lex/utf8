package io.gitlab.wulflex.utf8;

import java.io.IOException;
import java.io.InputStream;

public class UTF8SequenceInputStream extends InputStream {

	private final UnicodeSequenceGenerator generator;

	private final byte[] buffer = new byte[4];

	private int index;

	public UTF8SequenceInputStream(UnicodeSequenceGenerator generator) {
		this.generator = generator;
	}

	@Override
	public int read() throws IOException {
		if (index > 0) {
			return buffer[--index] & 0xff;
		}

		int next = generator.next();

		if (next == -1) {
			return -1;
		}

		index = utf8(next, buffer);

		return buffer[--index] & 0xff;
	}

//	private static int utf8(int codePoint, byte[] bytes) {
//		Character.toChars(codePoint);
//	}


	private static int utf8(int codePoint, byte[] bytes) {
		if (codePoint < 0x80) {
			bytes[0] = (byte) codePoint;
			return 1;
		}

		final int length;

		if (codePoint < 0x800) {
			length = 2;
		} else if (codePoint < 0x10000) {
			length = 3;
		} else {
			length = 4;
		}

		int i = 0;
		while(i < length - 1) {
			bytes[i++] = (byte)((codePoint & 0b0011_1111) | 0b1000_0000);
			codePoint >>= 6;
		}

		bytes[i] = (byte)(codePoint | (-1 << (8 - length) & 0xFF));

		return length;
	}
}
