package io.gitlab.wulflex.utf8;

public class ValidUnicodeSequenceGenerator implements UnicodeSequenceGenerator {

	private int current = -1;

	@Override
	public int next() {
		current++;

		if (current == 0xD800) {
			return current = 0xDFFF + 1;
		}

		if (current > 0x10FFFF) {
			return -1;
		}

		return current;
	}
}
