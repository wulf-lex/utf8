package io.gitlab.wulflex.utf8;

import io.gitlab.wulflex.CodePointReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

public class NaiveUTF8CodePointReader implements CodePointReader {

	private final Reader reader;

	public NaiveUTF8CodePointReader(InputStream inputStream) {
		this.reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
	}

	@Override
	public int read() throws IOException {
		int high = reader.read();

		if (high == -1 || !Character.isSurrogate((char) high)) {
			return high;
		}

		if (!Character.isHighSurrogate((char) high)) {
			throw new IOException("Invalid encoding");
		}

		int low = reader.read();
		if (low == -1) {
			return -1;
		}

		if (!Character.isLowSurrogate((char) low)) {
			throw new IOException("Invalid encoding");
		}

		return Character.toCodePoint((char) high, (char) low);
	}
}
