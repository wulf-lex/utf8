package io.gitlab.wulflex.utf8;

import io.gitlab.wulflex.CodePointReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class SimpleTest {

	@Test
	public void readAllValidCodePoints() throws IOException {
		InputStream inputStream = new UTF8SequenceInputStream(new ValidUnicodeSequenceGenerator());
		CodePointReader reader = new UTF8CodePointReader(inputStream);
		shouldReadAllValidUnicodeSequences(reader);
	}

	@Test
	public void readAllValidCodePointsShifting() throws IOException {
		InputStream inputStream = new UTF8SequenceInputStream(new ValidUnicodeSequenceGenerator());
		CodePointReader reader = new ShiftingUTF8CodePointReader(inputStream);
		shouldReadAllValidUnicodeSequences(reader);
	}

	@Test
	public void testAlternativeCodePointReaders() throws IOException {
		CodePointReader naiveUTF8CodePointReader = new NaiveUTF8CodePointReader(new UTF8SequenceInputStream(new ValidUnicodeSequenceGenerator()));
		shouldReadAllValidUnicodeSequences(naiveUTF8CodePointReader);

		CodePointReader neanderthalUTF8CodePointReader = new NeanderthalUTF8CodePointReader(new UTF8SequenceInputStream(new ValidUnicodeSequenceGenerator()));
		shouldReadAllValidUnicodeSequences(neanderthalUTF8CodePointReader);
	}

	private void shouldReadAllValidUnicodeSequences(CodePointReader reader) throws IOException {
		UnicodeSequenceGenerator generator = new ValidUnicodeSequenceGenerator();
		for (int i; (i = reader.read()) != -1; ) {
			Assertions.assertEquals(generator.next(), i);
		}
	}

	@Test
	public void shouldFailOnTooShort2ByteSequence() {
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xc2 });
	}

	@Test
	public void shouldFailOnTooShort3ByteSequence() {
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xe0, (byte)0xa0 });
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xe1, (byte)0x80 });
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xed, (byte)0x80 });
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xee, (byte)0x80 });
	}

	@Test
	public void shouldFailOnTooShort4ByteSequence() {
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x80 });
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x80 });
		shouldFailWithUnexpectedEndOfStream(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x80 });
	}

	private void shouldFailWithUnexpectedEndOfStream(byte[] bytes) {
		CodePointReader reader = new UTF8CodePointReader(new ByteArrayInputStream(bytes));

		IOException e = Assertions.assertThrows(IOException.class, reader::read);
		Assertions.assertEquals("Unexpected end of stream", e.getMessage());
	}

	@Test
	public void shouldFailOnMalformed1ByteSequence() {
		// Fail directly
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0x80 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xc1 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf5 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xff });
	}

	@Test
	public void shouldFailOnMalformed2ByteSequence() {
		// Fail after 1
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xc2, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xc2, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xc2, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xc2, (byte)0xff });
	}

	@Test
	public void shouldFailOnMalformed3ByteSequence() {
		// Fail after 1
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0x9f });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0xa0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0xff });

		// Fail after 2
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0xa0, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0xa0, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0xa0, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe0, (byte)0xa0, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xe1, (byte)0x80, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xed, (byte)0x80, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xee, (byte)0x80, (byte)0xff });
	}

	@Test
	public void shouldFailOnMalformed4ByteSequence() {
		// Fail after 1
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x8f });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x90 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0xff });

		// Fail after 2
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0xff });

		// Fail after 3
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf0, (byte)0x90, (byte)0x80, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf1, (byte)0x80, (byte)0x80, (byte)0xff });

		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x80, (byte)0x00 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x80, (byte)0x79 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x80, (byte)0xc0 });
		shouldFailWithIncorrectEncoding(new byte[] { (byte)0xf4, (byte)0x80, (byte)0x80, (byte)0xff });

	}

	private void shouldFailWithIncorrectEncoding(byte[] bytes) {
		CodePointReader reader = new UTF8CodePointReader(new ByteArrayInputStream(bytes));

		IOException e = Assertions.assertThrows(IOException.class, reader::read);
		Assertions.assertEquals("Incorrect UTF-8 encoding", e.getMessage());
	}

	@Disabled("This tests runs very long, so should only be run sporadically.")
	@Test
	public void readAllInvalidCodePoints() {
		InvalidUTF8SequenceInputStream inputStream = new InvalidUTF8SequenceInputStream();
		CodePointReader reader = new UTF8CodePointReader(inputStream);

		while (true) {
			int i;
			try {
				i = reader.read();
			} catch (IOException e) {
				// As expected, an exception was thrown.
				// Make sure the input stream is ready for the next code point.
				inputStream.clear();
				continue;
			}

			if (i == -1) {
				break;
			}

			Assertions.fail();
		}
	}
}
