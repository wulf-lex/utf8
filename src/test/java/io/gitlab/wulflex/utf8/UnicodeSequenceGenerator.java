package io.gitlab.wulflex.utf8;

public interface UnicodeSequenceGenerator {

	int next();
}
