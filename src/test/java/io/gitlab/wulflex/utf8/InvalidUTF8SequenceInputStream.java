package io.gitlab.wulflex.utf8;

import java.io.InputStream;

public class InvalidUTF8SequenceInputStream extends InputStream {

	private long current = -1;

	private int index = 0;

	private int[] buffer = new int[4];

	@Override
	public int read() {

		if (index == 0) {
			if (!fillBuffer()) {
				return -1;
			}
		}

		return buffer[--index];
	}

	private boolean fillBuffer() {
		current++;

		while (process(current)) {
			current++;
		}

		return current <= 0xffffffffL;
	}

	private boolean process(long sequence) {
		if (sequence > 0xffffffffL) {
			return false;
		}

		if ((sequence & 0xff000000L) > 0) {
			return process4Byte(sequence);
		}

		if ((sequence & 0xff0000L) > 0) {
			return process3Byte(sequence);
		}

		if ((sequence & 0xff00L) > 0) {
			return process2Byte(sequence);
		}

		if ((sequence & 0xffL) > 0 || sequence == 0) {
			return process1Byte(sequence);
		}

		return false;
	}

	private boolean process1Byte(long sequence) {
		if (isValid1Sequence(sequence)) {
			return true;
		}

		buffer[0] = (int)sequence;
		index = 1;
		return false;
	}

	private boolean process2Byte(long sequence) {
		long byte1 = (sequence & 0xff00) >> 8;
		long byte2 = sequence & 0x00ff;

		if (isValid1Sequence(byte1) || isValid2Sequence(byte1, byte2)) {
			return true;
		}

		buffer[0] = (int)byte2;
		buffer[1] = (int)byte1;
		index = 2;
		return false;
	}

	private boolean process3Byte(long sequence) {
		long byte1 = (sequence & 0xff0000) >> 16;
		long byte2 = (sequence & 0x00ff00) >> 8;
		long byte3 = sequence & 0x0000ff;

		if (isValid1Sequence(byte1) || isValid2Sequence(byte1, byte2) || isValid3Sequence(byte1, byte2, byte3))  {
			return true;
		}

		buffer[0] = (int)byte3;
		buffer[1] = (int)byte2;
		buffer[2] = (int)byte1;
		index = 3;
		return false;
	}

	private boolean process4Byte(long sequence) {
		long byte1 = (sequence & 0xff000000) >> 24;
		long byte2 = (sequence & 0x00ff0000) >> 16;
		long byte3 = (sequence & 0x0000ff00) >> 8;
		long byte4 = sequence & 0x000000ff;

		if (isValid1Sequence(byte1) || isValid2Sequence(byte1, byte2) || isValid3Sequence(byte1, byte2, byte3)
				|| isValid4Sequence(byte1, byte2, byte3, byte4))  {
			return true;
		}

		buffer[0] = (int)byte4;
		buffer[1] = (int)byte3;
		buffer[2] = (int)byte2;
		buffer[3] = (int)byte1;
		index = 4;
		return false;
	}

	private boolean isValid1Sequence(long byte1) {
		return isInRange(byte1, 0x00, 0x7f);
	}

	private boolean isValid2Sequence(long byte1, long byte2) {
		return isInRange(byte1, 0xc2, 0xdf)
				&& isInRange(byte2, 0x80, 0xbf);
	}

	private boolean isValid3Sequence(long byte1, long byte2, long byte3) {
		return
				(
						isInRange(byte1, 0xe0, 0xe0)
						&& isInRange(byte2, 0xa0, 0xbf)
						&& isInRange(byte3, 0x80, 0xbf)
				)
				||
				(
						isInRange(byte1, 0xe1, 0xec)
						&& isInRange(byte2, 0x80, 0xbf)
						&& isInRange(byte3, 0x80, 0xbf)
				)
				||
				(
						isInRange(byte1, 0xed, 0xed)
						&& isInRange(byte2, 0x80, 0x9f)
						&& isInRange(byte3, 0x80, 0xbf)
				)
				||
				(
						isInRange(byte1, 0xee, 0xef)
						&& isInRange(byte2, 0x80, 0xbf)
						&& isInRange(byte3, 0x80, 0xbf)
				);

	}

	private boolean isValid4Sequence(long byte1, long byte2, long byte3, long byte4) {
		return
				(
						isInRange(byte1, 0xf0, 0xf0)
						&& isInRange(byte2, 0x90, 0xbf)
						&& isInRange(byte3, 0x80, 0xbf)
						&& isInRange(byte4, 0x80, 0xbf)
				)
				||
				(
						isInRange(byte1, 0xf1, 0xf3)
						&& isInRange(byte2, 0x80, 0xbf)
						&& isInRange(byte3, 0x80, 0xbf)
						&& isInRange(byte4, 0x80, 0xbf)
				)
				||
				(
						isInRange(byte1, 0xf4, 0xf4)
						&& isInRange(byte2, 0x80, 0x8f)
						&& isInRange(byte3, 0x80, 0xbf)
						&& isInRange(byte4, 0x80, 0xbf)
				);
	}

	private boolean isInRange(long value, long min, long max) {
		return value >= min && value <= max;
	}

	public void clear() {
		index = 0;
	}
}
