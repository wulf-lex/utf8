package io.gitlab.wulflex.utf8;

public class UnicodeUtil {

	public static boolean isValidCodePoint(int codePoint) {
		if (codePoint < 0) {
			return false;
		}

		if (codePoint >= 0xD800 && codePoint <= 0xDFFF) {
			return false;
		}

		if (codePoint > 0x10FFFF) {
			return false;
		}

		return true;
	}

}
