package io.gitlab.wulflex.utf8;

import io.gitlab.wulflex.CodePointReader;

import java.io.IOException;
import java.io.InputStream;

public class ShiftingUTF8CodePointReader implements CodePointReader {

	private static final int[] CLASSES = new int[] {
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
			1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
			2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,
			3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
			3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,
			12, 12, 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
			4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
			5,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  6,  7,  8,  8,
			9, 10, 10, 10, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,
	};

	private static final int[][] STATES = new int[][] {
		/*    0  1  2  3  4  5  6  7  8  9  10 11 12        */
		/*    a  b  c  d  e  f  g  h  i  j  k  l  m         */
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 0 */
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 1 */
			{ 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 2 */
			{ 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 3 */
			{ 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 4 */
			{ 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 5 */
			{ 0, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 6 */
			{ 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 7 */
			{ 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, /* 8 */
			{ 1, 0, 0, 0, 2, 8, 3, 4, 3, 7, 6, 5, 0 }, /* 9 */
	};

	private static final int ACCEPTING_STATE = 1;

	private static final int ERROR_STATE = 0;

	private static final int START_STATE = 9;


	private final InputStream in;

	public ShiftingUTF8CodePointReader(final InputStream in) {
		this.in = in;
	}

	@Override
	public int read() throws IOException {
		int next = in.read();

		if (next == -1) {
			return -1;
		}

		int state = STATES[START_STATE][CLASSES[next]];

		if (state == ERROR_STATE) {
			throw new IOException("Incorrect UTF-8 encoding");
		}

		// If the state is already the accepting state, the code point is in the ascii range (i.e. 0x00 - 0x7f). Because
		// of this we can return the value without applying a mask.
		if (state == ACCEPTING_STATE) {
			return next;
		}

		int value = next & (0xff >> state);

		for (int i = 0; i < 3; i++) {
			next = in.read();
			if (next == -1) {
				throw new IOException("Unexpected end of stream");
			}

			state = STATES[state][CLASSES[next]];

			if (state == ERROR_STATE) {
				throw new IOException("Incorrect UTF-8 encoding");
			}

			value = (value << 6) | (next & 0x3f);

			if (state == ACCEPTING_STATE) {
				return value;
			}
		}

		throw new IOException("Incorrect UTF-8 encoding");
	}

}
