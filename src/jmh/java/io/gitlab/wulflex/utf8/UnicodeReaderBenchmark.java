package io.gitlab.wulflex.utf8;

import com.google.common.io.ByteStreams;
import io.gitlab.wulflex.CodePointReader;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


// ./gradlew --console=plain --no-daemon jmh

@Fork(5)
@Warmup(iterations = 5, time = 10000, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 10000, timeUnit = TimeUnit.MILLISECONDS)
@State(Scope.Thread)
public class UnicodeReaderBenchmark {

	public enum Type {
		ALL_CODE_POINTS_SEQUENTIAL,
		ALL_CODE_POINTS_RANDOM,
		ASCII_CODE_POINTS_RANDOM
	}

	@Param
	public Type dataSetType;

	public byte[] bytes;

	@Setup
	public void setup() throws IOException {
		if (dataSetType == Type.ALL_CODE_POINTS_SEQUENTIAL) {
			bytes = buildAllCodePointsSequential();
		} else if (dataSetType == Type.ALL_CODE_POINTS_RANDOM) {
			bytes = buildAllCodePointsRandom();
		} else if (dataSetType == Type.ASCII_CODE_POINTS_RANDOM) {
			bytes = buildAsciiCodePointsRandom();
		} else {
			throw new IllegalArgumentException("Wrong type");
		}
	}

	@Benchmark
	public void benchmarkUTF8CodePointReader() throws IOException {
		CodePointReader reader = new UTF8CodePointReader(createInputStream());
		while (reader.read() != -1) {
			// No body
		}
	}

	@Benchmark
	public void benchmarkShiftingUTF8CodePointReader() throws IOException {
		CodePointReader reader = new ShiftingUTF8CodePointReader(createInputStream());
		while (reader.read() != -1) {
			// No body
		}
	}

	@Benchmark
	public void benchmarkNaiveUTF8CodePointReader() throws IOException {
		CodePointReader reader = new NaiveUTF8CodePointReader(createInputStream());
		while (reader.read() != -1) {
			// No body
		}
	}

	@Benchmark
	public void benchmarkNeanderthalUTF8CodePointReader() throws IOException {
		CodePointReader reader = new NeanderthalUTF8CodePointReader(createInputStream());
		while (reader.read() != -1) {
			// No body
		}
	}

	private InputStream createInputStream() {
		return new NonSynchronizedByteArrayInputStream(bytes);
	}


	private static byte[] buildAllCodePointsSequential() throws IOException {
		UnicodeSequenceGenerator generator = new ValidUnicodeSequenceGenerator();

		InputStream inputStream = new UTF8SequenceInputStream(generator);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ByteStreams.copy(inputStream, outputStream);

		return outputStream.toByteArray();
	}

	private static byte[] buildAllCodePointsRandom() throws IOException {
		List<Integer> codePointList = new ArrayList<>();
		UnicodeSequenceGenerator generator = new ValidUnicodeSequenceGenerator();
		for (int i; (i = generator.next()) != -1; ) {
			codePointList.add(i);
		}

		final int[] codePointArray = codePointList.stream()
				.mapToInt(Integer::intValue)
				.toArray();

		shuffle(codePointArray);

		InputStream inputStream = new UTF8SequenceInputStream(new UnicodeSequenceGenerator() {
			int index = -1;
			@Override
			public int next() {
				index++;
				if (index >= codePointArray.length) {
					return -1;
				}
				return codePointArray[index];
			}
		});

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ByteStreams.copy(inputStream, outputStream);

		return outputStream.toByteArray();
	}

	private static void shuffle(int[] array) {
		int n = array.length;
		Random random = new Random(0L);

		for (int i = array.length - 1; i > 0; i--) {
			int index = random.nextInt(i + 1);
			int temp = array[index];
			array[index] = array[i];
			array[i] = temp;
		}
	}

	private static byte[] buildAsciiCodePointsRandom() {
		byte[] bytes = new byte[2_097_152];
		Random random = new Random(0L);
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) random.nextInt(0x80);
		}
		return bytes;
	}

}


// Benchmark                                                                    (dataSetType)   Mode  Cnt    Score   Error  Units
// UnicodeReaderBenchmark.benchmarkShiftingUTF8CodePointReader     ALL_CODE_POINTS_SEQUENTIAL  thrpt   25  113.857 ± 0.209  ops/s
// UnicodeReaderBenchmark.benchmarkShiftingUTF8CodePointReader         ALL_CODE_POINTS_RANDOM  thrpt   25  102.324 ± 0.471  ops/s
// UnicodeReaderBenchmark.benchmarkShiftingUTF8CodePointReader       ASCII_CODE_POINTS_RANDOM  thrpt   25  335.624 ± 0.224  ops/s
// UnicodeReaderBenchmark.benchmarkNaiveUTF8CodePointReader        ALL_CODE_POINTS_SEQUENTIAL  thrpt   25   22.363 ± 0.120  ops/s
// UnicodeReaderBenchmark.benchmarkNaiveUTF8CodePointReader            ALL_CODE_POINTS_RANDOM  thrpt   25   20.517 ± 0.292  ops/s
// UnicodeReaderBenchmark.benchmarkNaiveUTF8CodePointReader          ASCII_CODE_POINTS_RANDOM  thrpt   25   32.261 ± 0.594  ops/s
// UnicodeReaderBenchmark.benchmarkNeanderthalUTF8CodePointReader  ALL_CODE_POINTS_SEQUENTIAL  thrpt   25  113.475 ± 0.102  ops/s
// UnicodeReaderBenchmark.benchmarkNeanderthalUTF8CodePointReader      ALL_CODE_POINTS_RANDOM  thrpt   25   82.103 ± 0.071  ops/s
// UnicodeReaderBenchmark.benchmarkNeanderthalUTF8CodePointReader    ASCII_CODE_POINTS_RANDOM  thrpt   25  229.181 ± 0.145  ops/s
// UnicodeReaderBenchmark.benchmarkUTF8CodePointReader             ALL_CODE_POINTS_SEQUENTIAL  thrpt   25  111.787 ± 0.117  ops/s
// UnicodeReaderBenchmark.benchmarkUTF8CodePointReader                 ALL_CODE_POINTS_RANDOM  thrpt   25  102.508 ± 0.232  ops/s
// UnicodeReaderBenchmark.benchmarkUTF8CodePointReader               ASCII_CODE_POINTS_RANDOM  thrpt   25  335.730 ± 0.313  ops/s
