package io.gitlab.wulflex.utf8;

import java.io.InputStream;

public class NonSynchronizedByteArrayInputStream extends InputStream {

	private byte buf[];

	private int pos;

	private int count;

	public NonSynchronizedByteArrayInputStream(byte buf[]) {
		this.buf = buf;
		this.pos = 0;
		this.count = buf.length;
	}

	public int read() {
		return (pos < count) ? (buf[pos++] & 0xff) : -1;
	}
}